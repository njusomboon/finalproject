﻿using System;
using Spaceship;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

namespace Manager
{
    public class GameManager : MonoBehaviour
    {
        [SerializeField] private Button startButton;
        [SerializeField] private TextMeshProUGUI highScoreText;
        [SerializeField] private RectTransform dialog;
        [SerializeField] private PlayerSpaceship playerSpaceship;
        [SerializeField] private EnemySpaceship enemySpaceship;
        public event Action OnRestarted;
        private int scoreN;
        private int sceneN;
        [SerializeField] private int playerSpaceshipHp;
        [SerializeField] private int playerSpaceshipMoveSpeed;
        [SerializeField] private int enemySpaceshipHp;
        [SerializeField] private int enemySpaceshipMoveSpeed;
        [SerializeField] private Button restartButton;
        [SerializeField] private Button nextButton;

        public static GameManager Instance { get; private set; }

        public void Awake()
        {
            if (Instance != null && Instance != this)
            {
                Destroy(this.gameObject);
                return;
            }
            if (Instance == null)
            {
                Instance = this;
                DontDestroyOnLoad(this.gameObject);
            }
            
            
            Debug.Assert(startButton != null, "startButton cannot be null");
            Debug.Assert(dialog != null, "dialog cannot be null");
            Debug.Assert(playerSpaceship != null, "playerSpaceship cannot be null");
            Debug.Assert(enemySpaceship != null, "enemySpaceship cannot be null");
            Debug.Assert(playerSpaceshipHp > 0, "playerSpaceship hp has to be more than zero");
            Debug.Assert(playerSpaceshipMoveSpeed > 0, "playerSpaceshipMoveSpeed has to be more than zero");
            Debug.Assert(enemySpaceshipHp > 0, "enemySpaceshipHp has to be more than zero");
            Debug.Assert(enemySpaceshipMoveSpeed > 0, "enemySpaceshipMoveSpeed has to be more than zero");
            startButton.onClick.AddListener(OnStartButtonClicked);
            nextButton.onClick.AddListener(NextScene);
            restartButton.onClick.AddListener(Restart);
        }

        private void OnStartButtonClicked()
        {
            dialog.gameObject.SetActive(false);
            StartGame();
        }

        private void NextScene()
        {
            sceneN += 1;
            var res = sceneN % 2;
            SceneManager.LoadScene(res == 0 ? "Game" : "Game2");
            dialog.gameObject.SetActive(false);
            Invoke(nameof(SpawnEnemySpaceship),0.1f);
        }
        private void StartGame()
        {
            SpawnPlayerSpaceship();
            SpawnEnemySpaceship();
            startButton.gameObject.SetActive(false);
        }
        
        //Player
        private void SpawnPlayerSpaceship()
        {
            var spaceship = Instantiate(playerSpaceship);
            spaceship.Init(playerSpaceshipHp, playerSpaceshipMoveSpeed);
            spaceship.OnExploded += OnPlayerSpaceshipExploded;
        }

        private void OnPlayerSpaceshipExploded()
        {
            dialog.gameObject.SetActive(true);
            restartButton.gameObject.SetActive(true);
            nextButton.gameObject.SetActive(false);
            highScoreText.text = $"High Score : {scoreN}";
        }
        private void SpawnEnemySpaceship()
        {
            var spaceship = Instantiate(enemySpaceship);
            spaceship.Init(enemySpaceshipHp, enemySpaceshipMoveSpeed);
            spaceship.OnExploded += OnEnemySpaceshipExploded;
        }
        private void OnEnemySpaceshipExploded()
        {
            scoreN += 1;
            ScoreManager.Instance.SetScore(scoreN);
            dialog.gameObject.SetActive(true);
            nextButton.gameObject.SetActive(true);
            startButton.gameObject.SetActive(false);
        }

        private void Restart()
        {
            sceneN = 0;
            scoreN = 0;
            SceneManager.LoadScene("Game");
            restartButton.gameObject.SetActive(false);
            startButton.gameObject.SetActive(true);
            ScoreManager.Instance.SetScore(scoreN);
            OnRestarted?.Invoke();
        }
    }
}
