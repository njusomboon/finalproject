using System;
using Manager;
using UnityEngine;
using UnityEngine.UI;

namespace Spaceship
{
    public class PlayerSpaceship : BaseSpaceship, IDamagable
    {
        public event Action OnExploded;
        [SerializeField] private Image hPImage;
        [SerializeField] private Bullet bullet2;
        private Bullet myBullet;
        private void Awake()
        {
            Debug.Assert(defaultBullet != null, "defaultBullet cannot be null");
            Debug.Assert(gunPosition != null, "gunPosition cannot be null");
        }
        
        public void Init(int hp, float speed)
        {
            base.Init(hp, speed, defaultBullet);
            FullHp = Hp;
            DontDestroyOnLoad(gameObject);
        }

        public void Fire1()
        {
            myBullet = defaultBullet;
            Fire();
        }
        public void Fire2()
        {
            myBullet = bullet2;
            Fire();
        }

        public override void Fire()
        {
            var bullet = Instantiate(myBullet, gunPosition.position, Quaternion.identity);
            AudioManager.Instance.Play("Laser1");
            bullet.Init();
        }

        public void TakeHit(int damage)
        {
            Hp -= damage;
            hPImage.fillAmount = Hp / FullHp;
            if (Hp > 0)
            {
                return;
            }
            Explode();
        }

        public void Explode()
        {
            Debug.Assert(Hp <= 0, "HP is more than zero");
            AudioManager.Instance.Play("Explosion2");
            Destroy(gameObject);
            OnExploded?.Invoke();
            Invoke(nameof(OnDestroy), 0.05f);
        }
        
        private void OnDestroy()
        {
            Destroy(gameObject);
        }
    }
}