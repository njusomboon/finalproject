﻿using Spaceship;
using UnityEngine;

namespace Enemy
{
    public class EnemyController : MonoBehaviour
    {
        public float rateOfFire;
        [SerializeField] private EnemySpaceship enemySpaceship;
        private float rate;

        private void Update()
        {
            Fire(rateOfFire);
        }

        private void Fire(float rOF)
         {
             rate -= Time.deltaTime;
             if (!(rate <= 0)) return;
             rate = rOF;
             enemySpaceship.Fire();
         }
    }    
}

